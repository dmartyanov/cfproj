package models.zakupki

import java.util.Date
import org.squeryl.annotations.Column

/**
 * User: hellraiser
 * Date: 11/30/13
 * Time: 2:06 PM
 */

class Notification(
                    val id: String,
                    val notificationType: String,
                    val originalId: String,
                    val href: String,
                    val notificationNumber: String,
                    val creationDate: Date,
                    val orderName: String,
                    @Column("notificationOrder") val order: String,
                    val orderPlacerRegNum: String,
                    val contactInfo: String,
                    val createDate: Date,
                    val documentMetasDocumentMeta: String,
                    val foundationNotificationNumber: String,
                    val modification: String,
                    val placingWay: String,
                    val printForm: String,
                    val publishDate: Date,
                    val publishVersionDate: Date,
                    val versionNumber: String,
                    val lots: String,
                    val notificationCancelFailure: String,
                    val notificationCommission: String,
                    val competitiveDocumentProvisioning: String,
                    val notificationPlacement: String,
                    val ep: String,
                    val json: String

                    )
{
  def entityDescription: String = orderName

  def entityTitle: String = s"Извещение №$notificationNumber"

  def title: String = new StringBuilder()
    .append(orderName)
    .toString();

  def content: String = new StringBuilder()
    .append(notificationNumber).append(" ")
    .append(contactInfo).append(" ")
    .append(lots).append(" ")
    .toString();
}
