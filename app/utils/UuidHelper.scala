package utils

import java.util.UUID

object UuidHelper {

    def randomUuid = UUID.randomUUID().toString.replaceAll("-", "")

}
