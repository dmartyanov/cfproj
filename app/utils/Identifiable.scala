package utils

import org.squeryl.KeyedEntity

trait Identifiable extends KeyedEntity[String]{

    override def hashCode = id.hashCode

    override def equals(other: Any) = other match {
        case that: Identifiable => this.id == that.id
        case _ => false
    }

}
