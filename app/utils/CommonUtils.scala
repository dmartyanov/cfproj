package utils


trait CommonUtils {

    def randomUuid = java.util.UUID.randomUUID.toString.replaceAll("-", "")

    def nonEmpty(s: String) = s != null && s.trim.nonEmpty

    def someNonEmpty(s: String) = Option(s).filter(_.trim.nonEmpty)

    def wordsSetFromString(wordsString: String) = wordsString.split("[,]").map(_.trim).filter(_.nonEmpty).toSet

}

object CommonUtils extends CommonUtils
