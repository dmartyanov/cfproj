package utils

import play.api.Play.current
import com.typesafe.plugin._
import scala.concurrent.{ExecutionContext, Future}
import play.api.Logger
import solr.TenderSolr

object MailHelper {

    import ExecutionContext.Implicits.global

    def sendMail(email: String, subject: String, content: String) = Future {

        val mail = use[MailerPlugin].email

        mail.setSubject(subject)
        mail.setRecipient(email)
        mail.setFrom("XPIR <xpir.system@gmail.com>")

        try {
            mail.sendHtml(content)
            Logger.info(s"sended mail to $email with subject $subject")
        } catch {
            case e: Exception => {
                Logger.error("Can't send email: " + e.getMessage)
            }
        }

    }

    def sendUserNotification = {
        TenderSolr.userNotification.map { case (user, tenders) =>
            sendMail(user.email, "Для Вас доступна новая информация", views.html.user.notificationMail(user, tenders).body)
        }
    }

}
