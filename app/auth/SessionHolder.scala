package auth

import play.api.cache.Cache
import play.api.Play.current
/**
 *
 */
object SessionHolder {
    private[auth] val sessionIdSuffix = ":sessionId"
    private[auth] val userIdSuffix = ":userId"

    def sessionId2userId(sessionId: String): Option[String] =
        Cache.get(sessionId + sessionIdSuffix).map(_.asInstanceOf[String])

    def prolongTimeout(sessionId: String, timeoutInSeconds: Int) {
        sessionId2userId(sessionId).foreach(addSessionToUser(sessionId, _, timeoutInSeconds))
    }

    def addSessionToUser(sessionId: String, userId: String, timeoutInSeconds: Int) = {
        Cache.set(sessionId + sessionIdSuffix, userId, timeoutInSeconds)
        Cache.set(
            userId.toString + userIdSuffix,
            (sessionsBy(userId) :+ sessionId).distinct,
            timeoutInSeconds)

    }

    def exists(sessionId: String) = sessionId2userId(sessionId).isDefined

    private def sessionsBy(userId: String) = {
        Cache.getAs[Seq[String]](userId.toString + userIdSuffix).getOrElse(Seq())
    }


    // дальнейшие методы не используются

    def userId2sessionId(userId: String): Option[String] =
        Cache.getAs[String](userId.toString + userIdSuffix)

    def removeBySessionId(sessionId: String) {
        sessionId2userId(sessionId) foreach unsetUserId
        unsetSessionId(sessionId)
    }
    def removeByUserId(userId: String) {
        println("removeByUserId(userId: Id)" + userId)
        sessionsBy(userId).map(unsetSessionId)
        unsetUserId(userId)
    }
    private[auth] def unsetSessionId(sessionId: String) {
        Cache.remove(sessionId + sessionIdSuffix)
    }
    private[auth] def unsetUserId(userId: String) {
        Cache.remove(userId.toString + userIdSuffix)
    }
}
