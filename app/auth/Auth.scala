package auth

import play.api.mvc._
import java.security.Principal
import dao.UserDao


case class RequestContext(request: Request[AnyContent], principal: Principal) extends WrappedRequest(request)

case object Anonymous extends Principal {
    def getName: String = toString
}

case class UserPrincipal(userId: String) extends Principal {
    def getName = toString

    lazy val user = UserDao.get(userId)//models.User(id = "abc", email = "mail@example.com", password = "pass", title = "ФИО", isAdmin = false)
}

object AuthHelper {
    implicit def principalToUserPrincipal(principal: Principal): UserPrincipal = principal match {
        case up: UserPrincipal => up
    }
}

sealed trait UserRole
object AuthorizedUser extends UserRole
object Admin extends UserRole
