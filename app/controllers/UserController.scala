package controllers

import auth.Admin
import dao.UserDao
import models.User
import auth.AuthHelper._
import utils.MailHelper

object UserController extends AuthorizedController with FormHelper {

    def addUser = authorizedAction(Admin) { implicit rc =>
         Ok(views.html.user.addUser())
    }

    def submitAddUser = authorizedAction(Admin) { implicit rc =>
        val form = urlEncodedForm(rc)
        val email = stringParam(form, "email")
        val password = stringParam(form, "password")
        val title = stringParam(form, "title")
        UserDao.add(User(
            id = utils.CommonUtils.randomUuid,
            email = email,
            password = password,
            title = title,
            isAdmin = false
        ))
        //Redirect(routes.UserCo)
        NoContent
    }

    def get(userId: String) = authorizedAction() { implicit rc =>
        val user = UserDao.get(userId)
        Ok(user.toString)
    }

    def me = authorizedAction() { implicit rc =>
        Ok(views.html.user.user(rc.principal.user))
    }

    def keywords = authorizedAction() { implicit rc =>
        Ok(views.html.user.userKeywords(rc.principal.user))
    }

    def results = authorizedAction() { implicit rc =>
        Ok(views.html.user.userResults(rc.principal.user))
    }

    def sendMessages  = authorizedAction() { implicit rc =>
        MailHelper.sendUserNotification
        Redirect(routes.UserController.keywords)
    }

    def sendFakeEmail = authorizedAction() { implicit rc =>

        val fakeEmailContent = views.html.fakeEmail().body/*
            """
              | 1) рекламные кампании в интернете<br>
              | 2) проектирование и разработка интернет-проектов;<br>
              | 3) разработка приложений для мобильных устройств;<br>
              | 4) развитие интернет-проектов;<br>
              | 5) обслуживание компьютерных и серверных систем, услуги по защите персональных данных;<br>
              | 6) разработка игр для социальных сетей.
            """.stripMargin*/

        MailHelper.sendMail(rc.principal.user.email, "Уведомления", fakeEmailContent)
        Redirect(routes.UserController.me)
    }

}
