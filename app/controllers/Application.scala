package controllers

import play.api.mvc._
import org.apache.tika.Tika
import models._
import play.api.libs.Files.TemporaryFile
import textprocessing.TextProcessor
import org.apache.tika.metadata.Metadata
import org.apache.tika.io.TikaInputStream

object Application extends AuthorizedController with FormHelper {

    def index = authorizedAction() { implicit rc =>
        Ok(views.html.index())
    }

    def stemFile = authorizedAction() { implicit rc =>
        Ok(views.html.stemFile())
    }

    val tika = new Tika()
    tika.setMaxStringLength(-1)
		
    def submitStemFile = authorizedAction() { implicit rc =>
        val (file, form) = multipartForm(rc)
		val dictionary = stringParam(form, "dictionary")
        val words = tokensFromFile(file)
        Ok(views.html.fileContent(dao.DictionaryDao.get(dictionary), file.filename, wordWithCount(words)))
    }

    private def tokensFromFile(file: MultipartFormData.FilePart[TemporaryFile]) = {
        val metadata = new Metadata
        val stream = TikaInputStream.get(file.ref.file.toURI.toURL, metadata)
        metadata.set(org.apache.tika.metadata.TikaMetadataKeys.RESOURCE_NAME_KEY, file.filename)
        val content = tika.parseToString(stream, metadata)
        TextProcessor.tokenizeAndStem(content)
    }

    def stemText = authorizedAction() { implicit rc =>
        Ok(views.html.stemText("", Seq()))
    }

    private def wordWithCount(words: Seq[String]) =
        words.groupBy(identity).map(w => w._1 -> w._2.size).toSeq.sortBy(-_._2)//.map(w => s"${w._1} (${w._2})")

    def submitStemText = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val text = stringParam(form, "word")
        val words = TextProcessor.tokenizeAndStem(text)

        Ok(views.html.stemText(text, wordWithCount(words)))
    }

    def checkFile = authorizedAction() { implicit rc =>
         Ok(views.html.checkFile(None, Seq()))
    }

    def submitCheckFile = authorizedAction() { implicit rc =>
        val (file, form) = multipartForm(rc)
        val dictionaryCode = stringParam(form, "dictionary")
        val words = tokensFromFile(file)
        val itemsMap = dao.DictionaryDao.get(dictionaryCode).items.map(di => (di.code, di)).toMap
        val check = Checker.check(words, itemsMap)
        val sumW = check.map(_.wordsWeight).sum.toDouble
        val rows = check.map(c => (c.wordsWeight.toDouble*100/sumW, c)).toSeq
            .sortBy(-_._1)
            .map { case (w,c) =>
                CheckResult(itemsMap(c.code).title, w, c.wordsWeight)
            }
        Ok(views.html.checkFile(Some(file.filename), rows))
    }

}

case class CheckResult(title: String, weight: Double, sum: Int)