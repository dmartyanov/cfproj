package controllers

import play.api.mvc._
import auth._
import concurrent.Future
import auth.UserPrincipal
import scala.annotation.tailrec
import concurrent.ExecutionContext.Implicits.global

/**
 * Трейт для контроллера с авторизованным доступом.
 */
trait AuthorizedController extends Controller {

    protected def authorizedAction(userRole: UserRole = AuthorizedUser)(action: RequestContext => SimpleResult) = {
        userAction { rc =>
            val hasPrivilege = rc.principal match {
                case up: UserPrincipal =>
                    userRole match {
                        case AuthorizedUser => true
                        case Admin => up.user.isAdmin
                    }
                case Anonymous => false
            }
            if (hasPrivilege) Future(action(rc)) else Future.successful(Redirect(routes.LoginController.login(rc.uri)))
        }
    }

    protected def userActionSync(f: RequestContext => SimpleResult): Action[AnyContent] = {
        userAction {rc => Future(f(rc))}
    }

    protected def userAction(f: RequestContext => Future[SimpleResult]): Action[AnyContent] = {
        Action.async(BodyParsers.parse.anyContent) { implicit req =>
            val principal = principalFromSession(req.session)
            val contextWrapper = RequestContext(req, principal)
            f(contextWrapper)
        }
    }

    protected def newSession[A](userId: Option[String])(implicit request: Request[A]) = {
        val sessionId = generateSessionId(request)
        userId.foreach(id => SessionHolder.addSessionToUser(sessionId, id, sessionTimeoutInSeconds))
        Session() + ("sessionId" -> sessionId)
    }

    private val random = new util.Random(new java.security.SecureRandom())

    @tailrec
    protected final def generateSessionId[A](implicit request: Request[A]): String = {
        val table = "abcdefghijklmnopqrstuvwxyz1234567890-_.!~*'()"
        val token = Stream.continually(random.nextInt(table.size)).map(table).take(64).mkString
        if (SessionHolder.exists(token)) generateSessionId(request) else token
    }

    /**
     * The session timeout in seconds
     */
    private val sessionTimeoutInSeconds: Int = 3600

    def userIdFromSession(session: Session) = {
        for {
            sessionId <- session.get("sessionId")
            userId <- SessionHolder.sessionId2userId(sessionId)
        } yield {
            SessionHolder.prolongTimeout(sessionId, sessionTimeoutInSeconds)
            userId
        }
    }

    def principalFromSession(session: Session) = {
        userIdFromSession(session) match {
            case Some(userId) => UserPrincipal(userId)
            case None => Anonymous
        }
    }

}
