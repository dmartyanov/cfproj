package controllers

import play.api.mvc._
import play.api.libs.Files.TemporaryFile


trait FormHelper {

    protected def multipartForm(r: Request[AnyContent]) = {
        val data: MultipartFormData[TemporaryFile] = r.body.asMultipartFormData.get
        val file = data.files(0)
        val form: Map[String, Seq[String]] = data.dataParts
        (file, form)
    }

    protected def urlEncodedForm(r: Request[AnyContent]) = r.body.asFormUrlEncoded.get

    protected def stringParam(form: Map[String, Seq[String]], paramName: String) = form.get(paramName).get.apply(0)

    protected def stringParamOpt(form: Map[String, Seq[String]], paramName: String) = form.get(paramName).map(_.apply(0))

}
