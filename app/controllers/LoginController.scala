package controllers

import play.api.mvc.Action
import dao.UserDao
import models.User
import utils.{MailHelper, UuidHelper}


object LoginController extends AuthorizedController with FormHelper {

    def login(backUrl: String) = userActionSync { implicit rc =>
        Ok(views.html.user.login(backUrl))
    }

    def submitLogin(backUrl: String) = Action { implicit req =>
        val form = urlEncodedForm(req)
        val email = stringParam(form, "email")
        val password = stringParam(form, "password")
        UserDao.findByEmail(email).flatMap(u =>
            if (u.password == password) Some(Redirect(backUrl).withSession(newSession(Some(u.id))))
            else None
        ).getOrElse(Redirect(backUrl))
    }

    def submitRegistration(backUrl: String) = Action { implicit req =>
        val form = urlEncodedForm(req)
        val email = stringParam(form, "email")
        val title = stringParam(form, "title")
        val password = stringParam(form, "password")
        val user = UserDao.add(User(id = UuidHelper.randomUuid, email, password, title, false))

        MailHelper.sendMail(user.email, "Добро пожаловать в систему XPIR", views.html.user.regEmail(user).body)

        Redirect(backUrl).withSession(newSession(Some(user.id)))
    }

}
