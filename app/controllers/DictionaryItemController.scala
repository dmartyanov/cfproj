package controllers

import play.api.mvc._
import models._
import utils.CommonUtils
import dao._
import textprocessing.TextProcessor


object DictionaryItemController extends AuthorizedController with FormHelper {

    def dictionaryItem(diId: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.dictionaryItem(DictionaryItemDao.get(diId)))
    }

    def submitAdd(dictionaryCode: String) = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val code = stringParam(form, "code")
        val title = stringParam(form, "title")
        val id = DictionaryItemDao.add(DBDictionaryItem(
            id = CommonUtils.randomUuid,
            dictionaryCode = dictionaryCode,
            code = code,
            title = title)
        ).id
        Redirect(routes.DictionaryItemController.dictionaryItem(id))
    }

    def delete(diId: String) = authorizedAction() { implicit rc =>
        val item = DictionaryItemDao.get(diId)
        item.delete()
        Redirect(routes.DictionaryController.dictionary(item.dictionaryCode))
    }

    def deleteWord(wordId: String) = authorizedAction() { implicit rc =>
        val word = WordDao.get(wordId)
        word.delete()
        Redirect(routes.DictionaryItemController.dictionaryItem(word.dictionaryItemId))
    }

    def addWord(itemId: String) = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val word = stringParam(form, "word")
        val stemmed = TextProcessor.tokenizeAndStem(word).mkString(" ")
        val weight = stringParam(form, "weight").toInt
        WordDao.add(Word(
            id = CommonUtils.randomUuid,
            dictionaryItemId = itemId,
            stemmed = stemmed,
            original = word,
            weight = weight
        ))
        Redirect(routes.DictionaryItemController.dictionaryItem(itemId))
    }

    def addWordAjax(itemId: String, word: String, weight: Int) = authorizedAction() { implicit rc =>
        val stemmed = TextProcessor.tokenizeAndStem(word).mkString(" ")
        WordDao.add(Word(
            id = CommonUtils.randomUuid,
            dictionaryItemId = itemId,
            stemmed = stemmed,
            original = word,
            weight = weight
        ))
        Ok("Слово добавлено")
    }

}
