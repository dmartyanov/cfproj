package controllers

import play.api.libs.json.Json
import solr.TenderSolr
import auth.AuthHelper._
import solr.TenderSolrConstants._

case class ResultsWrapper(keywords: String, title: String, content: String)

object ResultsController extends AuthorizedController with FormHelper {

    private implicit val resultsWrites = Json.writes[ResultsWrapper]

    def list = authorizedAction() { implicit rc =>

        Ok(Json.toJson[Seq[ResultsWrapper]](TenderSolr.userNotification.filter(_._1 == rc.principal.user).map { case (user, keywords) =>
            keywords.map { case (w, tenders) =>
                tenders.map { tender =>
                    ResultsWrapper(w.content, tender.id, tender.content
                        .replaceAll(HighlightPre, "<span style='background-color: #D6E714; font-weight: bold;'>")
                        .replaceAll(HighlightPost, "</span>")
                        .replaceAll(HighlightDelimeter, "<br />")
                    )
                }
            }.flatten
        }.flatten)).as("application/json")

    }

}
