package controllers

import play.api.libs.json.{JsSuccess, JsValue, Reads, Json}
import models.Keywords
import dao.KeywordsDao
import auth.AuthHelper._
import utils.UuidHelper

object KeywordsController extends AuthorizedController with FormHelper {

    private implicit val keywordsWrites = Json.writes[Keywords]
    private implicit val testObjectReads = new Reads[Keywords] {
        def reads(json: JsValue) = {
            JsSuccess(Keywords(
                (json \ "id").asOpt[String].getOrElse(UuidHelper.randomUuid),
                (json \ "content").as[String],
                (json \ "userId").asOpt[String].getOrElse(null)
            ))
        }
    }


    def list = authorizedAction() { implicit rc =>

        Ok(Json.toJson[Seq[Keywords]](KeywordsDao.list(rc.principal.user.id))).as("application/json")

    }

    def add = authorizedAction() { implicit rc =>

        rc.body.asJson.map { j =>
            val words = j.as[Keywords].copy(id = UuidHelper.randomUuid, userId = rc.principal.user.id)
            KeywordsDao.add(words)
            Ok(Json.toJson(words)).as("application/json")
        } getOrElse BadRequest

    }

    def delete(id: String) = authorizedAction() { implicit rc =>

        KeywordsDao.delete(id)

        Ok

    }

}
