package controllers

import auth.Admin
import solr._

/**
 * только для тестов
 */
object TenderSolrController extends AuthorizedController with FormHelper {

    def indexTender = authorizedAction(Admin) { implicit rc =>
        Ok(views.html.solr.indexTender())
    }

    def submitIndexTender = authorizedAction(Admin) { implicit rc =>
        val form = urlEncodedForm(rc)
        val id = stringParam(form, "id")
        val content = stringParam(form, "content")

        TenderSolr.index(TenderSolrModel(id, content))

        Redirect(controllers.routes.TenderSolrController.indexTender())
    }

    def search = authorizedAction() { implicit rc =>
        val q = rc.request.getQueryString("q").getOrElse{
            rc.body.asFormUrlEncoded.map(f => stringParamOpt(f, "q").getOrElse("")).getOrElse("")
        }
        val r = if(utils.CommonUtils.nonEmpty(q)) TenderSolr.search(q) else Seq()
        Ok(views.html.solr.tenderSearch(q, r))
    }

}
