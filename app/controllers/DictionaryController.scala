package controllers

import models._
import play.api.mvc._
import org.apache.commons.io.FileUtils
import dao.DictionaryDao

object DictionaryController extends AuthorizedController with FormHelper {

    def dictionaries = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.dictionariesList(DictionaryDao.list))
    }

    def dictionary(code: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.dictionary(DictionaryDao.get(code)))
    }

    def addDictionary = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.addDictionary())
    }

    def submitAddDictionary = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val code = stringParam(form, "code")
        val title = stringParam(form, "title")
        DictionaryDao.add(DBDictionary(code = code, title = title))
        Redirect(routes.DictionaryController.dictionary(code))
    }

    def deleteDictionary(code: String) = authorizedAction() { implicit rc =>
        DictionaryDao.get(code).delete
        Redirect(routes.DictionaryController.dictionaries)
    }

    def dictionaryStopWords(code: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.dictionaryStopWords(DictionaryDao.get(code)))
    }

    def editDictionaryStopWords(code: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.editDictionaryStopWords(DictionaryDao.get(code)))
    }

    def submitEditDictionaryStopWords(code: String) = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val text = stringParam(form, "word")
        DictionaryDao.saveDictionaryStopWords(code, text)
        Redirect(routes.DictionaryController.dictionaryStopWords(code))
    }

    def dictionaryProtWords(code: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.dictionaryProtWords(DictionaryDao.get(code)))
    }

    def editDictionaryProtWords(code: String) = authorizedAction() { implicit rc =>
        Ok(views.html.dictionary.editDictionaryProtWords(DictionaryDao.get(code)))
    }

    def submitEditDictionaryProtWords(code: String) = authorizedAction() { implicit rc =>
        val form = urlEncodedForm(rc)
        val text = stringParam(form, "word")
        DictionaryDao.saveDictionaryProtWords(code, text)
        Redirect(routes.DictionaryController.dictionaryProtWords(code))
    }

    def importXML = authorizedAction() { implicit rc =>
        Ok(views.html.importDictionary.importXml())
    }

    def submitImportXML = authorizedAction() { implicit rc =>
        val (file, form) = multipartForm(rc)
        val dictionaryCode = stringParam(form, "dictionary")
        DBDictionaryImporter.importDictionary(dictionaryCode, FileUtils.readFileToString(file.ref.file))
        Redirect(routes.DictionaryController.dictionaries)
    }

    def dictionaryXml(code: String) = authorizedAction() { implicit rc =>
        val d = DictionaryDao.get(code)
        val sb = new StringBuilder()
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>")
        d.items.foreach { i =>
            sb.append("\n\t<item code=\"" + i.code + "\" title=\"" + i.title + "\">")
            sb.append("\n\t\t<words>")
            i.wordsList.foreach( word =>
                sb.append("\n\t\t\t<word value=\"" + word.original + "\" weight=\"" + word.weight + "\"/>")
            )
            sb.append("\n\t\t</words>")
            sb.append("\n\t</item>")
        }
        sb.append("\n<root>")
        Ok(views.html.simple(sb.toString()))
    }

}
