package models

case class CheckItem(code: String, wordsWeight: Int)

object Checker {

    // TODO  закодить понятней
    def check(docWords: Seq[String], itemsMap: Map[String, DBDictionaryItem]): Seq[CheckItem] = {
        val dictItems = itemsMap.values.toList

        // для каждой тематики
        // делаем списки слов с группировкой по количеству слов
        // каждый список прогоняем по всем словам

        dictItems.foldLeft(Map[String, CheckItem]()) { (checkItems, dictItem) =>

            val wordCheckItems = dictItem.wordsWithCount.flatMap { case (count, words) =>
                val wordsMap = words.map(w => (w._1.mkString(" "), w._2)).toMap
                docWords.sliding(count, 1).map { docWordsSlide =>
                    CheckItem(
                        code = dictItem.code,
                        wordsWeight = wordsMap.get(docWordsSlide.mkString(" ")).map(w => w.weight).getOrElse(0)
                    )
                }
            }.filter(_.wordsWeight > 0)

            // CheckItem'ы суммируются
            wordCheckItems.foldLeft(checkItems) { (checkItems1, wci) =>
                checkItems1 + (wci.code -> {
                    checkItems1.get(wci.code).map(ci1 => ci1.copy(
                        wordsWeight = ci1.wordsWeight + wci.wordsWeight
                    )).getOrElse(wci)
                })
            }
        }.values.toList
    }

}