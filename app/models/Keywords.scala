package models

import utils.Identifiable

case class Keywords(id: String = utils.UuidHelper.randomUuid,
                    content: String,
                    userId: String) extends Identifiable
