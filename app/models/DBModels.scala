package models

import org.squeryl.KeyedEntity
import dao._
import org.squeryl.annotations.Transient
import textprocessing.TextProcessor


case class DBDictionary(code: String,
                        title: String,
                        stopWords: String = "",
                        protWords: String = "")
    extends KeyedEntity[String] {

    def id = code

    @Transient lazy val items = DictionaryDao.listDictionaryItems(code).sortBy(_.title)

    def clean() { items.foreach(_.delete()) }

    def delete() {
        clean()
        DictionaryDao.delete(id)
    }

}

case class DBDictionaryItem(id: String,
                            dictionaryCode: String,
                            code: String,
                            title: String)
    extends KeyedEntity[String] {

    lazy val dictionary = DictionaryDao.get(dictionaryCode)

    lazy val wordsList = WordDao.listByDictionaryItem(id).sortBy(-_.weight)

    lazy val words = WordDao.listByDictionaryItem(id).map(w => (w.stemmed, w)).toMap

    def wordsWithCount = wordsList.map(w => (TextProcessor.tokenize(w.stemmed), w)).groupBy(_._1.size)

        //.map(i => (i._1, i._2.map(_.mkString(" "))))

    def delete() {
        wordsList.foreach(_.delete())
        DictionaryItemDao.delete(id)
    }

}

case class Word(id: String,
                dictionaryItemId: String,
                stemmed: String,
                original: String,
                weight: Int)
    extends utils.Identifiable {

    def delete() { WordDao.delete(id) }

}