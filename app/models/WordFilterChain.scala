package models


object WordFilterChain extends WordFilter {

    private lazy val chain = Seq(OneSymbolFilter, YoFilter, StopWordsFilter, NumberFilter)

    def filter(word: String): Option[String] = {
        var current = word
        chain.foreach{f =>
            val r = f.filter(current)
            r match {
                case None => return None
                case Some(w) => current = w
            }
        }
        Some(current)
    }

}

trait WordFilter {

    def filter(word: String): Option[String]

}

object YoFilter extends WordFilter {

    def filter(word: String) = Some(word.replace("ё", "е"))

}

object OneSymbolFilter extends WordFilter {

    def filter(word: String) = if (word.length > 1) Some(word) else None

}

object StopWordsFilter extends WordFilter {

    lazy val StopWords = CFUtils.loadLuceneWordList("stopwords.txt")

    def filter(word: String) = if (StopWords.contains(word)) None else Some(word)

}

object NumberFilter extends WordFilter {

    def filter(word: String) = {
        if(word.filterNot(ch => ch.isDigit || ch == '.' || ch == ',').isEmpty) None else Some(word)
    }

}