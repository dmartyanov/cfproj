package models

import utils.Identifiable

/**
 * Пользователь
 */
case class User(id: String, email: String, password: String, title: String, isAdmin: Boolean) extends Identifiable
