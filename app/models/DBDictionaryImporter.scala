package models

import utils.CommonUtils
import dao._
import org.tartarus.snowball.ext.RussianStemmer

object DBDictionaryImporter {

    private def attr(node: xml.Node, name: String) = node.attribute(name).get.head.text

    def importDictionary(dictionaryCode:String, xmlString: String) {

        DictionaryDao.find(dictionaryCode).foreach(_.clean())

        val stemmer = new RussianStemmer
        (xml.XML.loadString(xmlString) \ "item").map { xmlItem =>
            val dbItem = DBDictionaryItem(
                id = CommonUtils.randomUuid,
                dictionaryCode = dictionaryCode,
                code = attr(xmlItem, "code"),
                title = attr(xmlItem, "title")
            )
            xmlItem \ "words" \ "word" map { xmlWord =>
                val original = attr(xmlWord, "value")
                stemmer.setCurrent(original)
                stemmer.stem()
                val stemmed = stemmer.getCurrent
                Word(
                    id = CommonUtils.randomUuid,
                    dictionaryItemId = dbItem.id,
                    stemmed = stemmed,
                    original = original,
                    weight = attr(xmlWord, "weight").toInt
                )
            } foreach WordDao.add
            DictionaryItemDao.add(dbItem)
        }
    }

}
