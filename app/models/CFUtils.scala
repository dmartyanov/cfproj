package models

import org.apache.commons.io.FileUtils
import config.AppConfig
import org.apache.lucene.analysis.util.WordlistLoader
import java.io.StringReader
import org.apache.lucene.util.Version

object CFUtils {

    def loadFileContent(fileName: String) = FileUtils.readFileToString(
        new java.io.File(AppConfig.configDirectoryPath + "/" + fileName))

    def loadLuceneWordList(fileName: String) = WordlistLoader.getSnowballWordSet(
        new StringReader(loadFileContent(fileName)), Version.LUCENE_44)
}
