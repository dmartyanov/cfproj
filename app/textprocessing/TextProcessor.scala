package textprocessing

import org.apache.lucene.analysis.standard.StandardTokenizer
import org.apache.lucene.util.Version
import java.io.StringReader
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.tartarus.snowball.ext.RussianStemmer
import models.{CFUtils, StopWordsFilter, WordFilterChain}
import collection.mutable.ArrayBuffer

/**
 *
 */
object TextProcessor {

    lazy val ProtectedWords = CFUtils.loadLuceneWordList("protwords.txt")

    def tokenize(text: String, filterFun: (String, RussianStemmer) => Option[String] = (w, s) => Some(w)) = {
        val tokenizer = new StandardTokenizer(Version.LUCENE_44, new StringReader(text))
        val charTermAttrib = tokenizer.getAttribute(classOf[CharTermAttribute])
        //TypeAttribute typeAtt = tokenizer.getAttribute(classOf[TypeAttribute])
        //OffsetAttribute offset = tokenizer.getAttribute(OffsetAttribute.class);

        val tokens = ArrayBuffer[String]()
        tokenizer.reset()
        val stemmer = new RussianStemmer
        while (tokenizer.incrementToken()) {
            val token = charTermAttrib.toString.toLowerCase
            filterFun(token, stemmer).foreach(t => tokens += t)
        }
        tokenizer.end()
        tokenizer.close()

        tokens.toList
    }

    def tokenizeAndStem(text: String) = {
        tokenize(text, { (word, stemmer) =>
            WordFilterChain.filter(word).flatMap { token =>
                if (ProtectedWords.contains(token)) Some(token)
                else {
                    stemmer.setCurrent(token)
                    stemmer.stem()
                    val stemmed = stemmer.getCurrent
                    if (!StopWordsFilter.StopWords.contains(stemmed)) Some(stemmed) else None
                }
            }
        })
    }
}
