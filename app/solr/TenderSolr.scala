package solr

import org.apache.solr.client.solrj.{SolrRequest => JSR}
import org.apache.solr.common.SolrInputDocument
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrServer
import dao.{UserDao, KeywordsDao}


case class TenderSolrModel(id: String, content: String)

object TenderSolr {

    private lazy val solrServer = new HttpSolrServer("http://localhost:9040/solr/Tender")

    def index(tender: TenderSolrModel) {

        val doc = new SolrInputDocument()
        doc.addField(TenderSolrConstants.IdField, tender.id)
        doc.addField(TenderSolrConstants.ContentField, tender.content)

        solrServer.add(doc)
        solrServer.commit()

    }

    def userNotification = KeywordsDao.list.groupBy(_.userId).map { case (userId, keywords) =>
        UserDao.get(userId) -> keywords.map(words => words -> search(words.content)).filter(_._2.nonEmpty)
    }.toList.filter(_._2.nonEmpty)




    def search(query: String): Seq[TenderSolrModel] = {

        import collection.convert.wrapAll._

        val sq = new SolrQuery(s"content:$query")
            .setHighlight(true)
            .setHighlightSnippets(3)
            .addHighlightField(TenderSolrConstants.ContentField)
            .setHighlightSimplePre(TenderSolrConstants.HighlightPre)
            .setHighlightSimplePost(TenderSolrConstants.HighlightPost)
            .setHighlightFragsize(1000)

        val response = solrServer.query(sq, JSR.METHOD.POST)
        val h = response.getHighlighting
        response.getResults.map(d => {
            val id = d.getFieldValue(TenderSolrConstants.IdField).toString
            //val content = d.getFieldValue(TenderSolrConstants.ContentField).toString
            val highlightSnippets = h.get(id).get(TenderSolrConstants.ContentField)
            TenderSolrModel(id = id, content = highlightSnippets.mkString(TenderSolrConstants.HighlightDelimeter))
        }).toList
    }

}

object TenderSolrConstants {

    val IdField = "id"
    val ContentField = "content"

    val HighlightPre = "__highlightPre__"
    val HighlightPost = "__highlightPost__"

    val HighlightDelimeter = "__HighlightDelimeter__"

}
