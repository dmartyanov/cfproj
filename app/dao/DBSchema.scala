package dao

import org.squeryl.Schema
import models._

object DBSchema extends Schema {

    val dictionary = table[DBDictionary]
    val dictionaryItem = table[DBDictionaryItem]
    val word = table[Word]

    val user = table[User]
    val keywords = table[Keywords]

}
