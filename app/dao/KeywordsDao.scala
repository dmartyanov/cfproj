package dao

import models.Keywords

object KeywordsDao extends SquerylSession {

    import org.squeryl.PrimitiveTypeMode._
    import DBSchema.keywords

    def list = tx { keywords.toList }

    def list(userId: String) = tx {

        from(keywords)(w =>
            where(w.userId === userId)
            select w
        ).toList

    }

    def add(words: Keywords) = tx { keywords.insert(words) }
    def delete(wordsId: String) = tx { keywords.delete(wordsId) }

}
