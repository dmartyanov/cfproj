package dao

import org.squeryl.PrimitiveTypeMode._
import models.User

object UserDao extends SquerylSession {

    def list = tx { DBSchema.user.toList }

    def get(userId: String) = tx {
        DBSchema.user.lookup(userId).get
    }

    def add(user: User) = tx {
        DBSchema.user.insert(user)
    }

    def delete(userId: String) = tx {
        DBSchema.user.delete(userId)
    }

    def findByEmail(email: String) = tx {
        from(DBSchema.user)(u => where(u.email === email) select u).headOption
    }

}
