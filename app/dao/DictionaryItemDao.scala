package dao

import models._

object DictionaryItemDao extends SquerylSession {

    import org.squeryl.PrimitiveTypeMode._

    def get(diId: String) = tx {
        DBSchema.dictionaryItem.lookup(diId).get
    }

    def add(dbItem: DBDictionaryItem) = tx {
        DBSchema.dictionaryItem.insert(dbItem)
    }

    def update(dbItem: DBDictionaryItem) = tx {
        DBSchema.dictionaryItem.update(dbItem)
    }

    def delete(id: String) = tx {
        WordDao.deleteByItem(id)
        DBSchema.dictionaryItem.delete(id)
    }

}