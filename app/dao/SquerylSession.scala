package dao

import org.squeryl.PrimitiveTypeMode._

trait SquerylSession {
    def tx[P](fun: => P) = inTransaction(fun)
}
