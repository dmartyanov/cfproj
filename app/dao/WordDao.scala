package dao

import org.squeryl.PrimitiveTypeMode._
import models._

object WordDao extends SquerylSession {

    def listByDictionaryItem(itemId: String) = tx {
        from(DBSchema.word)(w => where(w.dictionaryItemId === itemId) select w).toList
    }

    def get(wordId: String) = tx {
        DBSchema.word.lookup(wordId).get
    }

    def add(word: Word) = tx {
        DBSchema.word.insert(word)
    }

    def delete(wordId: String) = tx {
        DBSchema.word.delete(wordId)
    }

    def deleteByItem(itemId: String) = tx {
        DBSchema.word.deleteWhere(w => w.dictionaryItemId === itemId)
    }

}
