package dao

import models._
import org.squeryl.PrimitiveTypeMode._

object DictionaryDao extends SquerylSession {

    def list = tx { DBSchema.dictionary.toList }

    def find(code: String) = tx { DBSchema.dictionary.lookup(code) }

    def get(code: String) = find(code).get

    def add(d: DBDictionary) = tx { DBSchema.dictionary.insert(d) }

    def delete(code: String) = tx { DBSchema.dictionary.delete(code) }

    def saveDictionaryStopWords(code: String, stopWords: String) = tx {
        DBSchema.dictionary.update( d =>
            where(d.code === code)
                set (d.stopWords := stopWords)
        )
    }

    def saveDictionaryProtWords(code: String, protWords: String) = tx {
        DBSchema.dictionary.update( d =>
            where(d.code === code)
                set (d.protWords := protWords)
        )
    }

    def listDictionaryItems(dictionaryCode: String) = tx {
        from(DBSchema.dictionaryItem)( di =>
            where (di.dictionaryCode === dictionaryCode)
                select di
        ).toList
    }

}