package importer

import scala.collection.convert.WrapAsScala
;
import java.io.File
import play.api.Logger
import play.api.Play._
import scala.Predef._
import scala.util.{Failure, Success, Try}
import javax.xml.bind.JAXBContext
import solr.TenderSolrModel
import models.zakupki.Notification


/**
 * User: hellraiser
 * Date: 11/30/13
 * Time: 11:33 AM
 */

/*trait FileImporter extends WrapAsScala
{
  protected def logger: Logger;

  protected def inputFolderConfig: String;

  protected def inputFolder: String = configuration.getString(inputFolderConfig)
    .getOrElse(sys.error(s"You must specify '$inputFolderConfig' with files for import"))

  def importFile(file: File)

  def run() {
    recursiveListFiles(new File(inputFolder)) match {
      case Nil => logger.info("input folder is empty")
      case filesToImport =>
        filesToImport.foreach(importFile)
    }

  }

  private def recursiveListFiles(f: File): Seq[File] = {
    val these = Option(f.listFiles).map(_.toList).getOrElse(Nil)
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }
}

trait NotificationImporter[E] extends FileImporter
{
  protected def logger: Logger = Logger("NOTIFICATIONS")

  protected def inputFolderConfig: String = "input-folder"

  def listJaxbObjects(file: File): Seq[E]

  def jaxbToEntity(jaxbObject: E): Either[String, E];

  def printEntity(e : E)

  def importFile(file: File) = {

    listJaxbObjects(file).foreach(jaxbObj =>
      jaxbToEntity(jaxbObj).right.flatMap(processEntity) match {
        case Left(err) => logger.error(err)
        case _ => ()
      }
    )
  }

  def processEntity(entity: E): Either[String, Unit] = {
    Try(printEntity(entity)) match {
      case Success(_) => Right()
    }
  }

}

trait NullHelper {

  implicit class NullWrapper[T <: AnyRef](nullable: T) {
    def safe[R >: Null](foo: T => R): R = {
      Option(nullable).map(foo).orNull[R]
    }
  }

}

object NotificationImport extends FileImporter
  with NotificationImporter[Notification]
  with  NullHelper
{
  private val jc = JAXBContext.newInstance("models.zakupki.jaxb")
  private val unmarshaller = jc.createUnmarshaller()

  def listJaxbObjects(file: File): Seq[Notification] = {
    unmarshaller.unmarshal(file).asInstanceOf[Seq[Notification]]
  }

  def jaxbToEntity(jaxbObject: Notification): Either[String, TenderSolrModel] = {
    Try(extractNotification(jaxbObject)) match {
      case Success(_) => Right(extractNotification(jaxbObject));
      case Failure(ex) => Left(ex.getMessage)
    }
  }

  def printEntity(e: TenderSolrModel): Unit = {
    logger.warn(e.content)
  }

  def extractNotification(n: Notification): TenderSolrModel = {

    new TenderSolrModel(
      id = n.title,
      content = n.content
    )
  }

  def printEntity(e: Notification): Unit = {
    logger.warn(e.content)
  }
}*/




