package config

import play.api.Play._

/**
 * Конфиг приложения
 */
object AppConfig {

    def configDirectoryPath = configuration.getString("config-directory").get

}
