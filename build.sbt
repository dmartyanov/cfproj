name := "cfproj"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
		jdbc,
		cache,
        "com.jolbox" % "bonecp" % "0.8.0-rc3",
        "org.squeryl" %% "squeryl" % "0.9.5-6",
        "commons-io" % "commons-io" % "2.4",
		"org.apache.tika" % "tika-core" % "1.4",
		"org.apache.tika" % "tika-parsers" % "1.4",
		"org.apache.lucene" % "lucene-core" % "4.4.0",
		"org.apache.lucene" % "lucene-analyzers-common" % "4.4.0",
		"org.liquibase" % "liquibase-core" % "2.0.5",
		"postgresql" % "postgresql" % "9.1-901.jdbc4",
		"org.apache.solr" % "solr-solrj" % "4.2.0",
		"com.typesafe" %% "play-plugins-mailer" % "2.2.0"
)     

play.Project.playScalaSettings