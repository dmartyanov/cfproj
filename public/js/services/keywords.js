(function(angular) {
    'use strict';

    angular.module("keywordsService", ["ngResource"])
        .factory("Keywords", function ($resource) {
            return $resource(
                "/api/keywords/:id",
                {id: "@id"},
                {update: {method: "PUT"}}
            )
        });

})(angular);