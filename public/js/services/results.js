(function(angular) {
    'use strict';

    angular.module("resultsService", ["ngResource"])
        .factory("Results", function ($resource) {
            return $resource(
                "/api/results/:id",
                {id: "@id"},
                {update: {method: "PUT"}}
            )
        });

})(angular);
