(function(angular) {
    'use strict';

    angular.module("resultsApp", ["resultsService"])

        .controller("ResultsCtrl", function($scope, Results) {
            $scope.results = Results.query();
        });


})(angular);