(function(angular) {
    'use strict';

    angular.module("keywordsApp", ["keywordsService"])

        .controller("KeywordsCtrl", function($scope, Keywords) {
            $scope.keywords = Keywords.query();

            $scope.addKeywords = function() {
                Keywords.save($scope.newKeywords, function(obj) {
                    $scope.keywords.push(obj);
                    $scope.newKeywords = new Keywords;
                });
            };

            $scope.delete = function(obj) {
                obj.$delete(function() {
                    $scope.keywords = Keywords.query();
                })
            };

        });


})(angular);